# `euclid`

A D library for drawing SVG technical diagrams.  It's based on a collection of Scheme routines I developed over time while drawing diagrams for [my blog](https://theartofmachinery.com/) and for technical presentations.

*Currently very alpha*

## Why the name?

About two-and-a-half thousand years ago, Euclid gathered all the geometry he knew into thirteen volumes, all proven from five postulates.  Many call it the birth of formal mathemetical theory.

In any case, he definitely drew a lot of diagrams.  Maybe he would have appreciated a tool like this.
