module euclid.geom;

import std.algorithm.comparison : among;

struct Vec
{
pure:
	double x = 0.0, y = 0.0;

	this(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	this(double v)
	{
		x = v;
		y = v;
	}

	Vec opUnary(string s)() const if (s == "-")
	{
		return Vec(-x, -y);
	}

	Vec opBinary(string op)(Vec o) const if (op.among("+", "-"))
	{
		return Vec(mixin("x", op, "o.x"), mixin("y", op, "o.y"));
	}

	Vec opBinary(string op)(double a) const if (op.among("*", "/"))
	{
		return Vec(mixin("x", op, "a"), mixin("y", op, "a"));
	}

	Vec opBinaryRight(string op)(double a) const if (op.among("*", "/"))
	{
		return Vec(mixin("x", op, "a"), mixin("y", op, "a"));
	}

	Vec opOpAssign(string op)(Vec o) if (op.among("+=", "-="))
	{
		mixin("x", op, "o.x;");
		mixin("y", op, "o.y;");
	}

	Vec opOpAssign(string op)(double a) if (op.among("*=", "/="))
	{
		mixin("x", op, "a;");
		mixin("y", op, "a;");
	}
}

Vec maskX(Vec p, double y=0.0) pure
{
	return Vec(p.x, y);
}

Vec maskY(Vec p, double x=0.0) pure
{
	return Vec(x, p.y);
}

Vec scale(Vec p, double sx, double sy) pure
{
	return Vec(sx * p.x, sy * p.y);
}

struct Box
{
pure:
	Vec pos, dim;

	this(Vec pos, Vec dim)
	{
		this.pos = pos;
		this.dim = dim;
	}

	this(Vec dim)
	{
		this.pos = Vec(0.0);
		this.dim = dim;
	}

	this(double x, double y)
	{
		this.pos = Vec(0.0);
		this.dim = Vec(x, y);
	}

	double x1() const
	{
		return pos.x;
	}

	double y1() const
	{
		return pos.y;
	}

	double x2() const
	{
		return pos.x + dim.x;
	}

	double y2() const
	{
		return pos.y + dim.y;
	}

	double w() const
	{
		return dim.x;
	}

	double h() const
	{
		return dim.y;
	}
}

Box expand(Box b, Vec margin) pure
{
	return Box(b.pos - margin, b.dim + 2 * margin);
}

struct Line
{
pure:
	Vec start, end;

	double x1() const
	{
		return start.x;
	}

	double y1() const
	{
		return start.y;
	}

	double x2() const
	{
		return end.x;
	}

	double y2() const
	{
		return end.y;
	}
}

Vec interp(Line l, double f = 0.5) pure
{
	return l.start + f * (l.end - l.start);
}
