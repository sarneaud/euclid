module euclid.svg;

import std.algorithm : copy, each, map, sort;
import std.array;
import std.format : formattedWrite;
import std.range;
import std.stdio : File, stdout;
import std.traits;
import std.typecons : isTuple, tuple, Tuple;

import sumtype;

public import euclid.geom;

void renderToFile(IRenderSvg obj, File output = stdout)
{
	output.write(`<?xml version="1.0" encoding="UTF-8" standalone="no"?>`);
	obj.renderSvg(outputRangeObject!char(output.lockingTextWriter));
}

IRenderSvg svg(Box canvas, Vec margin, IRenderSvg[] children) pure
{
	auto my_attrs = attrs(
		"version", "1.1",
		"viewBox", canvas.expand(margin),
		"xmlns", "http://www.w3.org/2000/svg"
	);
	return new SvgElement("svg", my_attrs, children);
}

IRenderSvg circle(Vec pos, double r, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	if (pos.x != 0.0) my_attrs["cx"] = pos.x;
	if (pos.y != 0.0) my_attrs["cy"] = pos.y;
	if (r != 0.0) my_attrs["r"] = r;
	return new SvgElement("circle", my_attrs);
}

IRenderSvg defs(AttrValue[string] attrs, IRenderSvg[] children) pure
{
	return new SvgElement("defs", attrs, children);
}

IRenderSvg desc(string content, AttrValue[string] attrs = no_attrs) pure
{
	return new SvgElement("desc", attrs, [new InnerContent(content)]);
}

IRenderSvg ellipse(Vec pos, Vec r, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	if (pos.x != 0.0) my_attrs["cx"] = pos.x;
	if (pos.y != 0.0) my_attrs["cy"] = pos.y;
	my_attrs["rx"] = r.x;
	my_attrs["ry"] = r.y;
	return new SvgElement("ellipse", my_attrs);
}

IRenderSvg g(AttrValue[string] attrs, IRenderSvg[] children) pure
{
	return new SvgElement("g", attrs, children);
}

IRenderSvg line(Line l, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	if (l.x1 != 0.0) my_attrs["x1"] = l.x1;
	if (l.y1 != 0.0) my_attrs["y1"] = l.y1;
	if (l.x2 != 0.0) my_attrs["x2"] = l.x2;
	if (l.y2 != 0.0) my_attrs["y2"] = l.y2;
	return new SvgElement("line", my_attrs);
}

IRenderSvg marker(string id, Vec dim, Box view_box, Vec ref_point, AttrValue[string] attrs, IRenderSvg[] children) pure
{
	auto my_attrs = attrs.dup;
	my_attrs["id"] = id;
	my_attrs["markerWidth"] = dim.x;
	my_attrs["markerHeight"] = dim.y;
	if (ref_point.x != 0.0) my_attrs["refX"] = ref_point.x;
	if (ref_point.y != 0.0) my_attrs["refY"] = ref_point.y;
	my_attrs["viewBox"] = attrValue(view_box);
	return new SvgElement("symbol", my_attrs, children);
}

IRenderSvg path(ref PathBuilder pb, AttrValue[string] attrs = no_attrs) pure
{
	return path(pb.get, attrs);
}

IRenderSvg path(AttrValue[] path_spec, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	my_attrs["d"] = path_spec;
	return new SvgElement("path", my_attrs);
}

struct PathBuilder
{
pure:

	ref PathBuilder m(Vec p) return { return add(attrValue("m", p)); }
	ref PathBuilder M(Vec p) return { return add(attrValue("M", p)); }
	ref PathBuilder l(Vec p) return { return add(attrValue("l", p)); }
	ref PathBuilder L(Vec p) return { return add(attrValue("L", p)); }
	ref PathBuilder t(Vec p) return { return add(attrValue("t", p)); }
	ref PathBuilder T(Vec p) return { return add(attrValue("T", p)); }

	ref PathBuilder h(double v) return { return add(attrValue("h", v)); }
	ref PathBuilder H(double v) return { return add(attrValue("H", v)); }
	ref PathBuilder v(double v) return { return add(attrValue("v", v)); }
	ref PathBuilder V(double v) return { return add(attrValue("T", v)); }

	ref PathBuilder c(Vec c1, Vec c2, Vec target) return { return add(attrValue("c", c1, ",", c2, ",", target)); }
	ref PathBuilder C(Vec c1, Vec c2, Vec target) return { return add(attrValue("C", c1, ",", c2, ",", target)); }

	ref PathBuilder s(Vec c, Vec target) return { return add(attrValue("s", c, ",", target)); }
	ref PathBuilder S(Vec c, Vec target) return { return add(attrValue("S", c, ",", target)); }
	ref PathBuilder q(Vec c, Vec target) return { return add(attrValue("q", c, ",", target)); }
	ref PathBuilder Q(Vec c, Vec target) return { return add(attrValue("Q", c, ",", target)); }

	ref PathBuilder Z() return { return add(attrValue("Z")); }

	AttrValue[] get()
	{
		return _segments[];
	}

private:
	ref PathBuilder add(AttrValue v) return
	{
		_segments.put(v);
		return this;
	}

	Appender!(AttrValue[]) _segments;
}

IRenderSvg polygon(Vec[] points, AttrValue[string] attrs = no_attrs) pure
{
	return polyX("polygon", points, attrs);
}

IRenderSvg polyline(Vec[] points, AttrValue[string] attrs = no_attrs) pure
{
	return polyX("polyline", points, attrs);
}

private IRenderSvg polyX(string tag_name, Vec[] points, AttrValue[string] attrs) pure
{
	auto my_attrs = attrs.dup;
	my_attrs["points"] = attrValue(points.enumerate.map!(t => tuple(t[0] == 0 ? "" : " ", t[1].x, ",", t[1].y)));
	return new SvgElement(tag_name, my_attrs);
}

IRenderSvg rect(Box b, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	if (b.x1 != 0.0) my_attrs["x"] = b.x1;
	if (b.y1 != 0.0) my_attrs["y"] = b.y1;
	my_attrs["width"] = b.dim.x;
	my_attrs["height"] = b.dim.y;
	return new SvgElement("rect", my_attrs);
}

IRenderSvg style(string css, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	my_attrs["type"] = "text/css";
	return new SvgElement("style", my_attrs, [new InnerContent(css)]);
}

IRenderSvg symbol(string id, Vec dim, Box view_box, AttrValue[string] attrs, IRenderSvg[] children) pure
{
	auto my_attrs = attrs.dup;
	my_attrs["id"] = id;
	my_attrs["width"] = dim.x;
	my_attrs["height"] = dim.y;
	my_attrs["viewBox"] = attrValue(view_box);
	return new SvgElement("symbol", my_attrs, children);
}

IRenderSvg text(Vec pos, AttrValue[string] attrs, IRenderSvg[] children) pure
{
	auto my_attrs = attrs.dup;
	if (pos.x != 0.0) my_attrs["x"] = pos.x;
	if (pos.y != 0.0) my_attrs["y"] = pos.y;
	return new SvgElement("text", my_attrs, children);
}

IRenderSvg text(string content, Vec pos, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	if (pos.x != 0.0) my_attrs["x"] = pos.x;
	if (pos.y != 0.0) my_attrs["y"] = pos.y;
	return new SvgElement("text", my_attrs, [new InnerContent(content)]);
}

IRenderSvg textPath(AttrValue[string] attrs, IRenderSvg[] children) pure
{
	return new SvgElement("textPath", attrs, children);
}

IRenderSvg title(string content, AttrValue[string] attrs = no_attrs) pure
{
	return new SvgElement("title", attrs, [new InnerContent(content)]);
}

IRenderSvg tspan(string content, AttrValue[string] attrs = no_attrs) pure
{
	return new SvgElement("tspan", attrs, [new InnerContent(content)]);
}

IRenderSvg use(string href, Vec pos, Vec dim, AttrValue[string] attrs = no_attrs) pure
{
	auto my_attrs = attrs.dup;
	my_attrs["href"] = href;
	if (pos.x != 0.0) my_attrs["x"] = pos.x;
	if (pos.y != 0.0) my_attrs["y"] = pos.y;
	if (dim.x != 0.0) my_attrs["width"] = dim.x;
	if (dim.y != 0.0) my_attrs["height"] = dim.y;
	return new SvgElement("use", my_attrs);
}

alias AttrValue = SumType!(string, double, This[]);
enum no_attrs = (AttrValue[string]).init;
AttrValue attrValue(Args...)(Args args) pure if (Args.length > 1)
{
	AttrValue[Args.length] avs;
	foreach (idx, a; args)
	{
		avs[idx] = attrValue(a);
	}
	return AttrValue(avs[].dup);
}

AttrValue attrValue(TP)(TP v) pure if (isTuple!TP)
{
	return attrValue(v.expand);
}

AttrValue attrValue(Vec p) pure
{
	return attrValue(p.x, " ", p.y);
}

AttrValue attrValue(Box b) pure
{
	return attrValue(b.x1, " ", b.y1, " ", b.w, " ", b.h);
}

AttrValue attrValue(T)(T v) pure if (__traits(compiles, AttrValue(v)))
{
	return AttrValue(v);
}

AttrValue attrValue(R)(R vs) pure if (isInputRange!R && !isSomeString!R)
{
	return AttrValue(vs.map!(v => attrValue(v)).array);
}

AttrValue[string] attrs(Args...)(Args args) pure
{
	static assert (Args.length % 2 == 0, "Arguments must be an alternating list of strings and values");
	AttrValue[string] ret;
	static foreach (idx; iota(0, Args.length, 2))
	{
		ret[args[idx]] = attrValue(args[idx+1]);
	}
	return ret;
}

interface IRenderSvg
{
	void renderSvg(OutputRange!char output);
}

class SvgElement : IRenderSvg
{
	this(string tag_name, AttrValue[string] attrs, IRenderSvg[] children = []) pure
	{
		_tag_name = tag_name;
		_attrs = attrs;
		_children = children;
	}

	final void renderSvg(OutputRange!char output)
	{
		output.put('<');
		copy(_tag_name, output);
		foreach (key; _attrs.keys.sort)
		{
			output.put(' ');
			copy(key, output);
			output.put('=');
			output.put('"');
			output.writeAttr(_attrs[key]);
			output.put('"');
		}
		if (_children.empty)
		{
			copy("/>", output);
		}
		else
		{
			output.put('>');
			foreach (child; _children) child.renderSvg(output);
			copy("</", output);
			copy(_tag_name, output);
			output.put('>');
		}
	}

private:
	string _tag_name;
	AttrValue[string] _attrs;
	IRenderSvg[] _children;
}

IRenderSvg multi(R)(R elements) pure
{
	return new MultiSvg(elements);
}

class MultiSvg : IRenderSvg
{
	this(R)(R elements) pure
	{
		_elements = elements.array;
	}

	void renderSvg(OutputRange!char output)
	{
		foreach (element; _elements) element.renderSvg(output);
	}

private:
	IRenderSvg[] _elements;
}

class InnerContent : IRenderSvg
{
	this(string content) pure
	{
		_content = content;
	}

	void renderSvg(OutputRange!char output)
	{
		output.writeXmlEscaped(_content);
	}

private:
	string _content;
}

private:

void writeAttr(OutputRange!char output, AttrValue value)
{
	// TODO: precision and formatting control
	import std.conv : to;
	value.match!(
		(string s) => writeXmlEscaped(output, s),
		(double x) => copy(x.to!string, output),
		(AttrValue[] l) { l.each!(v => output.writeAttr(v)); return output; }
	);
}

OutputRange!char writeXmlEscaped(OutputRange!char output, string value)
{
	// This will escape some characters that don't need to be escaped
	foreach (char c; value)
	{
		switch (c)
		{
			case '&':
				copy("&amp;", output);
				break;

			case '<':
				copy("&lt;", output);
				break;

			case '>':
				copy("&gt;", output);
				break;

			case '"':
				copy("&quot;", output);
				break;

			case '\'':
				copy("&apos;", output);
				break;

			default:
				output.put(c);
		}
	}
	return output;
}
